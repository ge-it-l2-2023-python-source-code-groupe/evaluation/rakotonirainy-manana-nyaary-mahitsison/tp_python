#:::::::::::::::::::::::: Affichage dans l'intepreteur::::::::::
def Affichage():
    print(f"---ANSWER------")
    print(f" ======= 1+1 ::::=====> {1+1}")


# ::::::::::::::::::::::::Poly-A::::::::::::::::::
def Poly_A():
    print("--A*20--")
    print("===>","A"*20)

# :::::::::::::::::::::::Poly-A et poly-GC:::::::::
def Poly_A_Poly_GC():
    print("A*20 + GC *40")
    print("===>","A"*20 + "GC"*40)

# ::::::::::::::::::::::::Écriture formatée::::::::::::
def Ecriture_formatee():
    a,b,c ="Salut",102,10.318
    print("a\tb\tc")
    print("||\t||\t||")
    print(f"{a} , {b} , {c:.2f}")

# :::::::::::::::::::::::::Écriture formatée 2::::::::::::
def Ecriture_formatee_2():
    perc_GC = ((4500+2575)/14800)*100

    #Ecriture formate
    print("Ecriture formate ")
    print("Le pourcentage de GC est :{:.0f} %".format(perc_GC),"\nLe pourcentage de GC est :{:.1f} %".format(perc_GC),"\nLe pourcentage de GC est: {:.2f} %".format(perc_GC),"\nLe pourcentage de GC est: {:.3f} %".format(perc_GC))

    #Ecriture f-string
    print("\nEcriture f-string")
    print(f"Le pourcentage de GC est {perc_GC:.0f} %\nLe pourcentage de GC est {perc_GC:.1f} % \nLe pourcentage de GC est {perc_GC:.2f} %  \nLe pourcentage de GC est {perc_GC:.3f} %")

    
    