# :::::::::::::::::::::::::::::::::JOURS DE LA SEMAINE:::::::::::::::::::

def Jour_semaine():
    print(":::::::::::::Jour de la semaine::::::::::::::::")
    print(":::::::::::::::::ANSWER:::::::::::::::")
    jour_semaine =["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"]
    print(jour_semaine)

    for element in jour_semaine:
        if(element == "Lundi" or element=="Mardi" or element=="Mercredi" or element=="Jeudi"):
            print('Au travail :::::::: (Lunid,Mardi,Mercredi,Jeudi)')
        elif(element =="Vendredi"):
            print(":::::Chouette c'est Vendredi")
        else:
           print("Week-end jour de repos :::(Samedi,Dimanche)")

# :::::::::::::::::::::::::::Séquence complémentaire d'un brin d'ADN::::::::::::::::::::::::::
def Sequence_brin_ADN():
    print("::::::::::::::::Séquence complémentaire d'un brin d'ADN:::::::::::::::::::::")
    print(":::::::::::::ANSWER::::::::::::::")
    brin_adn = ["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C", "G"]
    print(f"Originale ====> {brin_adn}")
    brin_adn_new=[]
    for element in brin_adn:
        if(element=="A"):
            brin_adn_new.append("T")
        elif(element=="C"):
            brin_adn_new.append("G")
        elif(element=="G"):
            brin_adn_new.append("C")
        elif(element=="T"):
            brin_adn_new.append("A")
    print(f"Brin ADN complémentaire ====> {brin_adn_new} ")


# :::::::::::::::::::::::::::::::::::::::::::MINIMUM D'UN LISTE::::::::::::::::::::::::
def Minimum_list():
    print("::::::::::::::::::MINIMUM D'UN LISTE::::::::::::::")
    print(":::::::::: ::::::::    ANSWER:    ::::::::::::::::")
    liste= [8, 4, 6, 1, 5]
    print(f" Liste =====> {liste}")
    min=liste[0]
    for i in liste:
        if i<min:
            min =i
    print(f"Le plus petit valeur ===={min}")


# ::::::::::::::::::::::::::::::::::::::::::::::FREQUENCE D'UN ACIDE ANIME::::::::::::::::::::::
def Frequence_acide_anime():
    print("")
    print(":::::::::::::::::::::::::::FREQUENCE D'UNACIDE ANIME:::::::::::::::::::::")
    print("")
    liste =["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]
    
    print(f"Liste ==== {liste}")
    print("")

    alanine_count =0
    arginine_count = 0
    tryptophane_count=0
    glycine_count = 0
    total_count = len(liste)
    for i in liste:
        if(i=="A"):
            alanine_count+=1
        elif(i=="R"):
            arginine_count+=1
        elif(i=="W"):
            tryptophane_count+=1
        elif(i=="G"):
            glycine_count+=1
    print(f"===> Nombre de alanine(A) est {alanine_count}, La frequence  === {(alanine_count/total_count)*100:.2f} %\n")
    print(f"===> Nombre de arginine(R) est {arginine_count}, La frequence === {(arginine_count/total_count)*100:.2f} %\n")
    print(f"===> Nombre de tryptophane(W) est {tryptophane_count},La frequence === {(tryptophane_count/total_count)*100:.2f} %\n")
    print(f"===> Nombre de glycine(G) est {glycine_count},La frequence === {(glycine_count/total_count)*100:.2f} %\n")
    


# :::::::::::::::::::::::::::::::::NOTE ET MENTION D'UN ÉTUDIANT ::::::::::::::::::::::
def Mention_etudiant():
    print("")
    print("::::::::::::::::::::NOTE ET MENTION::::::::::::::::\n")
    note = [14, 9, 13, 15 , 12]
    print(f"Note ====> {note}\n")
    note_min = min(note)
    note_max = max(note)

    moyenne = (note_max+note_min)/2

    print("====> La moyenne  est de :",moyenne,"\n")

    if(moyenne>=10 and moyenne<12):
        print("======> Mention : Passable \n")
    elif(moyenne>=12 and moyenne<14):
        print("=====> Mention :Assez bien \n")
    elif(moyenne>=14):
        print("====> MENTION: Bien \n")



# :::::::::::::::::::::::::::::::::::::::NOMBRE PAIR et IMPAIRE :::::::::::::::::::::::::::
def Nombre_pair():
    print("")
    print(":::::::::::::::::Nombre pair[0,10] et impair[11,20]::::::::::::::::::::::::::\n")
    nombre_pair =[]
    nombre_impair=[]

    for i in range(0,21):
        if(i%2==0 and i<=10):
            nombre_pair.append(i)
        if(i%2!=0 and i>10):
            nombre_impair.append(i)
    print(f"======> Nombre pair [0,10] === {nombre_pair} \n ")
    print(f"======> Nombre impair [11,20] === {nombre_impair} \n ")

# :::::::::::::::::::::::::::::::::::::::CONJECURE DE SYCARUS ::::::::::::::::::::::::::::::::
def Syracuse():
    print("")
    print("::::::::::::::::::::CONJECTURE DE SYCARUCE::::::::::::::::::::\n")
    nbr =int(input(" ===> Entrer un nombre : "))
    
    while nbr != 1:
        print(f" > {nbr} ")
        if nbr % 2 == 0:
            nbr = nbr // 2
        else:
            nbr = 3 * nbr + 1



#::::::::::::::::::::::::::::::::::::: ANGLE :::::::::::::::::::::::::::::::::::::::::::::::
def Angle():

    print(""" 
          ****************************************************************************
          Attribution de la Structure Secondaire des acides aminés d'une protéines 
          ****************************************************************************
          """)

    proteine_1TFE =[[48.6, 53.4],[-124.9, 156.7],[-66.2, -30.8], \
                    [-58.8, -43.1],[-73.9, -40.6],[-53.7, -37.5], \
                    [-80.6, -26.0],[-68.5, 135.0],[-64.9, -23.5], \
                    [-66.9, -45.5],[-69.6, -41.0],[-62.7, -37.5], \
                    [-68.2, -38.3],[-61.2, -49.1],[-59.7, -41.1]]

    


    Phi_parfait, Psi_parfait = -57, -47

    for acide_amine in proteine_1TFE:
        if ((Phi_parfait-30<=acide_amine[0]<=Phi_parfait+30 and Psi_parfait-30<=acide_amine[1]<=Psi_parfait+30)):
            print(f'{acide_amine}   : est en Hélice ')
        else : print(f"======> {acide_amine}   : n'est pas en Hélice ")

    

# :::::::::::::::::::::::::::::::::::::::::::::NOMBRE PREMIER INFÉRIEUR À 100:::::::::::::::::::::
def Nombre_premier():
    print("")
    print("::::::::::::::::::::::::::NOMRE PREMIER INFERIEUR À 100 ::::::::::::::::::::\n")
    prime_numbers = []
    i = 2

    while i <= 100:
        is_prime = True

        for prime in prime_numbers:
            if i % prime == 0:
                is_prime = False
                break

        if is_prime:
            prime_numbers.append(i)

        i += 1
    
    print(f"Nombre premier ===========> {prime_numbers} \n")


# :::::::::::::::::::::::::::::::::::::::::::::::NOMBRE PAR DICHOTOMIE:::::::::::::::::::::
def Nombre_dichotomie():
    import random

    def get_answer():
        answer = random.randint(1, 100)
        return answer

    def compute_midpoint(low, high):
        midpoint = low + (high - low) / 2
        return midpoint

    def get_user_input(prompt):
        user_input = input(prompt)
        return user_input

    def compare_input(user_input, midpoint):
        if user_input == 'p':
            return -1
        elif user_input == 'g':
            return 1
        elif user_input == 'e':
            return 0
        else:
            print(" ==> Entrée invalide, veuillez entrer 'p' pour plus petit, 'g' pour plus grand ou 'e' pour égal.")
            return None

   
    answer = get_answer()
    low = 1
    high = 100

    while True:
        midpoint = compute_midpoint(low, high)
        print(f"Est-ce que le nombre que vous avez pensé est plus petit, plus grand ou égal à {midpoint} ? \n")
        user_input = get_user_input("Entrez 'p' pour plus petit, 'g' pour plus grand ou 'e' pour égal : ")
        print("")
        comparison_result = compare_input(user_input, midpoint)

        if comparison_result is None:
            continue

        if comparison_result == 0:
            print(f" =====>  C'est le nombre auquel j'avais pensé. Bravo ! Le nombre est {midpoint}. \n")
            break
        elif comparison_result == 1:
            low = midpoint + 1
        else:
            high = midpoint - 1


