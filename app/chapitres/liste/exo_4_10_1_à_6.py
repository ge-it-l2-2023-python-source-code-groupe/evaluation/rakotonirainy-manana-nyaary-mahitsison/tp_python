# ::::::::::::::::::::::::Jours de la semaine:::::::::::::::
def Jour_semaine():
    liste =["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"]
    print(f"Jour de la semaine ::: {liste}")
    print("")
    print("::::::ANSWER 1:::::")
    print(f"Cinq premier jour : {liste[0:5]}")
    print(f"Jour week-end : {liste[5:7]}")
    print("")
    print("::::::ANSWER 2:::::")
    print(f"Cinq premier jour : {liste[:5]}")
    print(f"Jour week-end : {liste[-2:]}")
    print("")
    print("::::::ANSWER 3:::::")
    print(f"Dernier_jour_semaine = {liste[-1]}")
    print(f"Dernier_jour_semaine_2 = {liste[6:7]}")
    print("")
    print("::::::ANSWER 4::::::")
    print(f"Inverse_jour_semaine = {liste[::-1]}")
    
def Saison():
    hivers =["Décembre","Janvier","Fevrier"]
    printemps =["Mars","Avril","Mai"]
    ete =["Juin","Juillet","Aout"]
    automne=["Septembre","Octobre","Novembre"]
    saison =[hivers,printemps,ete,automne]
    print(f"""
Hivers : {hivers}
Printemps: {printemps}
Éte : {ete}
Automne :{automne}
Saison : {saison}""")
    print("")
    
    print(":::::::ANSWER 1:::::")
    print(f"saison[2] ==== {saison[2]}\n")
    print(":::::::ANSWER 2:::::")
    print(f"saison[1][0] ==={saison[1][0]}\n")
    print(":::::::ANSWER  3::::")
    print(f"saison[1:2] === {saison[1:2]}\n")
    print(":::::::ANSWER 4:::::")
    print("saison[:][1] ==== Il parcoure toutes les tableau et affiche uniquement Printemps")

def Table_multiplication_9():
    print(":::::Table de multiplication de 9 :::::")
    table= list(i*9 for i in range(0,11))
    print(f"=====> {table}")

def length_nombre_pair():
    length = len(range(2,10000,2))+1
    print("======================================================")
    print(f"Le nombre de nombre pair entre [2,10000] est {length}")
    print("======================================================")


def List_indice():
      liste =["Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"]
      print(liste)
      print("")
      print("::::::ANSWER 1:::::")
      print(liste)
      print("")
      print("::::::ANSWER 2::::::")
      print(f"Valeur de semaine[4::::> {liste[4]}")
      print("")
      print("::::::ANSWER 3::::::")
      liste[0],liste[-1]=liste[-1],liste[0]
      print(f"Echange valeur ===> \n Premier liste=[0]=Lundi et Dernier liste[-1]=Dimanche \n ====> Premier liste[0]=Dimanche et Dernier liste [-1] = Lundi")
      print("")
      print(":::::: ANSWER 4::::::")
      print("Affichage de dernier élément de la liste en 12 fois ")
      print(f"======> {liste[-1]*12}")
    
def List_range():
    IstVide=[]
    IstFlottant=[0.0]*5
    print(":::::: ANSWER 1:::::")
    print(f"\tIstvide ===>{IstVide}")
    print(f"\tIstFlotant ===>{IstFlottant}")
    print("")
    print(":::::: ANSWER 2:::::")
    print(" : Ajout de 0 à 1000 avec step 200 au IstVide :")
    IstVide=[i for i in range(0,10001,200)]
    print(f" \t=====̾>{IstVide}")
    print("")
    print("::::::ANSWER 3::::::")
    liste_new=[i for i in range(0,4)]
    liste_new_2=[i for i in range(4,8)]
    liste_new_3=[i for i in range(2,9,2)]
    print(f"""
          Les entier de 0 à 3 ===>{liste_new}
          Les entiers de 4 à 7 ===> {liste_new_2}
          Les entier de 2 à 8 ===>{liste_new_3}""")
    print("")
    print("::::: ANSWER 4::::::")
    IstElmnt=[i for i in range(0,6)]
    print(" ::Ajout du contenu IstVide et IstFloat à la fin de  IstElmnt::")
    IstElmnt.append(IstVide)
    IstElmnt.append(IstFlottant)
    print(f"\tIstElmnt ====>{IstElmnt}")

    

    
      
    