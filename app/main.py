from tools.console import clear
from pynput import keyboard

def on_press(key):
    if key == keyboard.Key.enter:
        return False
def key():
     with keyboard.Listener(on_press=on_press) as Listener:
                  print("\n############################Toucher ENTER########################## ")
                  Listener.join()
                  clear()
def Menu():
    print("""
    #############################################
    #############################################
    ~~~RAKOTONIRAINY Manana Ny Aary Mahitsison~~~
    ~~~~~~~~~~~~~~~~~~~ N ° 9 ~~~~~~~~~~~~~~~~~~~ 
    #############################################
    #############################################
          """)
    print("""\n
    :::::::::::::::::::::::::::::::::::::::::::::
    :::::::::::::::::::::MENU::::::::::::::::::::
    :::::::::::::::::::::::::::::::::::::::::::::""")
    print("""
          1) Chap 2:: Variables
          2) Chap 3:: Affichage
          3) Chap 4:: Listes
          4) chap 5:: Boucle et comparaison
          5) chap 6 : Tests
          6) Quitter
          """)
def chap1():
    print("~~~~~~~~~~~CHAP 2 : VARIABLE ~~~~~~~")
    print("""
         1) Affiche de 1+1
         2) Nombre de Friedman
         3) Prédire le résultat : Opérations
         4) Prédire le résultat : Opérations et conversions de types
         5) Retour au Menu  
          """)   
def chap2():
    print("~~~~~~~~~~~CHAP 3: AFFICHAGE~~~~~~~~")
    print("""
    
    1) Poly-A
    2) Poly-A et Poly_GC
    3) Ecriture Formatée 1
    4) Ecriture formatée 2
    5) Retour Menu
          """) 
def chap3():
    print("~~~~~~~~~~~CHAP 4: LISTE~~~~~~~~~~~~")
    print("""
    1) Semaine
    2) Saison
    3) Table de multiplicationde 9 (list ,range)
    4) Nombre de nombre pair entre [2,10000]
    5) Liste & Indice
    6) List & Range
    7) Retour Menu
          """) 
def chap4():
    print("~~~~~~~~~~~CHAP 5 : BOUCLE ET COMPARAISON ~~~~~~~~~~")
    print("""
    1) Boucle de base
    2) Boucle et jour de la semaine
    3) Nombre de 1 à 10 sur une ligne
    4) Nombre pairs et impairs
    5) Calcul de la moyenne
    6) Produit de nombre consécutifs
    7) Triangle
    8) Triangle inversé 
    9) Triangle gauche
    10) Pyramide
    11) Parcour de matrice
    12) Parcours de demi-maitrice sans la diagonale
    13)  Saut de puce
    14) Suite de Fibonacci
    15) Retour au menu principale
          """) 
def chap5():
     print("~~~~~~~~~~~CHAP 6 : TESTS ~~~~~~~~~~")
     print("""
        1) Jour de la semaine
        2) Séquence complémentaire d'un brin d'ADN
        3) Minimum d'un liste
        4) Fréquence d'acide aminés
        5) Notes et mention d'un étudiant
        6) Nombres pairs et impairs
        7) Conjecture de Syracuse
        8) Angle
        9) Détermination des nombres premiers inférieurs à 100
        10) Recherche d'un nombre par dichotomie
        11) Retour au menu principale
            """) 
        
    
"""_summary_
Endpoint 
"""
if __name__=="__main__":
    from chapitres.variables.exo_2_11_1_à_3 import*
    from chapitres.affichage.exo_3_6_1_à_5 import *
    from chapitres.liste.exo_4_10_1_à_6 import *
    from chapitres.Boucle_et_comparaison.exo_5_4_1_à_14 import *
    from chapitres.Testes.exo_6_7_1_à_10 import *
  
    while True:
        Menu()
        choix=input("--Choix:::> ")
        print("")
        clear()
        if choix =="1":
           while True:
               chap1()
               choix_1=input("====> ")
               print("")
               clear()
               if choix_1=="1":
                    Affichage()
                    key()
                    continue
               elif choix_1 =="2":
                   Friedman()
                   key()
                   continue
               elif choix_1=="3":
                   Prediction()
                   key()
                   continue
               elif choix_1=="4":
                   Prediction_2()
                   key()
                   continue
               elif choix_1=="5":
                   print("===> Retour au Menu \n")
                   key()
                   break
            
        elif choix=="2":
            while True:
                chap2()
                choix_2=input("=====> ")
                print("")
                clear()
                if choix_2=="1":
                    Poly_A()
                    key()
                    continue
                elif choix_2=="2":
                    Poly_A_Poly_GC()
                    key()
                    continue
                elif choix_2=="3":
                    Ecriture_formatee()
                    key()
                    continue
                elif choix_2=="4":
                    Ecriture_formatee_2()
                    key()
                    continue
                elif choix_2=="5":
                    print("===> Retour au Menu \n ")
                    key()
                    break
        elif choix=="3":
            while True:
                chap3()
                choix_3=input("=====> ")
                print("")
                clear()
                if choix_3 =="1":
                    Jour_semaine()
                    key()
                    continue
                elif choix_3=="2":
                    Saison()
                    key()
                    continue
                elif choix_3=="3":
                    Table_multiplication_9()
                    key()
                    continue
                elif choix_3=="4":
                    length_nombre_pair()
                    key()
                    continue
                elif choix_3=="5":
                    List_indice()
                    key()
                    continue
                elif choix_3=="6":
                    List_range()
                    key()
                    continue
                elif choix_3=="7":
                    print("===> Retour au Menu \n")
                    key()
                    break
                    
        elif choix=="4":
            while True:
                chap4()
                choix_4=input("=====> ")
                print("")
                clear()
                if choix_4=="1":
                    Boucle_base()
                    key()
                    continue
                elif choix_4=="2":
                    Boucle_jour_semaine()
                    key()
                    continue
                elif choix_4=="3":
                    Nombre_1_10()
                    key()
                    continue
                elif choix_4=="4":
                    Nombre_pair_imapir()
                    key()
                    continue
                elif choix_4=="5":
                    Calcul_moyenne()
                    key()
                    continue
                elif choix_4=="6":
                    Produit_nombre_consecutif()
                    key()
                    continue
                elif choix_4=="7":
                    Triangle()
                    key()
                    continue
                elif choix_4=="8":
                    Triangle_inverse()
                    key()
                    continue
                elif choix_4=="9":
                    Triangle_gauche()
                    key()
                    continue
                elif choix_4=="10":
                    Pyramide()
                    key()
                    continue
                elif choix_4=="11":
                    Parcour_matrice()
                    key()
                    continue
                elif choix_4=="12":
                    Parcour_demi_matrice()
                    key()
                    continue
                elif choix_4=="13":
                    Saut_de_puce()
                    key()
                    continue
                elif choix_4=="14":
                    Suite_fibonnacci()
                    key()
                    continue
                elif choix_4=="15":
                    print("===> Retour au Menu \n ")
                    key()
                    break
        elif choix=="5":
            while True:
                chap5()
                choix_5=input("=====> ")
                print("")
                clear()
                if choix_5=="1":
                    Jour_semaine()
                    key()
                    continue
                elif choix_5=="2":
                    Sequence_brin_ADN()
                    key()
                    continue
                elif choix_5=="3":
                    Minimum_list()
                    key()
                    continue
                elif choix_5=="4":
                    Frequence_acide_anime()
                    key()
                    continue
                elif choix_5=="5":
                    Mention_etudiant()
                    key()
                    continue
                elif choix_5=="6":
                    Nombre_pair()
                    key()
                    continue
                elif choix_5=="7":
                    Syracuse()
                    key()
                    continue
                elif choix_5 =="8":
                    Angle()
                    key()
                    continue
                elif choix_5=="9":
                    Nombre_premier()
                    key()
                    continue
                elif choix_5=="10":
                    Nombre_dichotomie()
                    key()
                    continue
                elif choix_5=="11":
                    print("===> Retour au Menu \n ")
                    key()
                    break
     
       
        elif choix=="6":
            print("""
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:::::::::::::::::::::BYE BYE BYE BYE BYE BYE::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                  """)
            key()
            break
       
        
            
            
    
  
    
  
